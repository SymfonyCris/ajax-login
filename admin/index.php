<!doctype html>
<?php
require('./libs/check-login.php');

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="">
    <?php include "./admin-components/include-components.php"; ?>
    <title>Document</title>
</head>
<body>
<div class="wrapper ">


<?php include "./admin-components/sidebar.php"; ?>
    <!-- ##### START ---- CONTENT ###### -->
    <div class="container">
        <?php var_dump($_SESSION)?>

        <!-- ##### START ---- BREADCUMB ###### -->
            <?php include "./admin-components/bread-crumb.php"; ?>
        <!-- ##### END ---- BREADCUMB ###### -->

        <!-- ##### START ---- CARDS ###### -->
            <div class="row ">
                <div class=" mb-2 col-lg-4 col-md-12 col-sm-12">
                    <div class="card ">
                        <div class="card-body">
                            <h5 class="card-title">Блог</h5>
                                <ul class="list-group">
                                    <li class="list-group-item "><a href="./add-article.php">Добавить новость</a></li>
                                    <li class="list-group-item"><a href="">Редактировать новость</a></li>
                                </ul>
                        </div>
                    </div>
                </div>
                <div class=" mb-2 col-lg-4 col-md-12 col-sm-12">
                    <div class="card ">
                        <div class="card-body">
                            <h5 class="card-title">Портфолио</h5>
                                <ul class="list-group">
                                    <li class="list-group-item "><a href="#">Добавить новость</a></li>
                                    <li class="list-group-item"><a href="">Редактировать новость</a></li>
                                </ul>
                        </div>
                    </div>
                </div>
                <div class=" mb-2 col-lg-4 col-md-12 col-sm-12">
                    <div class="card ">
                        <div class="card-body">
                            <h5 class="card-title">Обратная связь</h5>
                                <ul class="list-group">
                                    <li class="list-group-item "><a href="#">Сообщения</a></li>
                                    <li class="list-group-item"><a href="">Заявки</a></li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        <!-- ##### END ---- CARDS ###### -->

        <!-- ##### START ---- INFO-TAB ###### -->
            <div class="info-tab">
                <!--Navigation-->
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
                           aria-controls="nav-home" aria-selected="true">Общая статистика сайта</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
                           aria-controls="nav-profile" aria-selected="false">Система</a>
                    </div>
                </nav>
                <!--Content-->
                <div class="tab-content" id="nav-tabContent">
                    <!--Content NAV ### 1 ###-->
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <table>
                            <tr>
                                <td>Общее количество проектов</td>
                                <td>1</td>
                            </tr>
                            <tr>
                                <td>Общее количество новостей</td>
                                <td>5</td>
                            </tr>
                            <tr>
                                <td>Поступившие жалобы</td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td>Общий размер кэша</td>
                                <td>510kb</td>
                            </tr>
                        </table>
                    </div>
                    <!--Content NAV ### 2 ###-->
                    <div class="tab-pane fade show " id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <table>
                            <tr>
                                <td>Общее количество aaaa</td>
                                <td>1</td>
                            </tr>
                        </table>
                    </div>
                </div>
        </div>
        <!-- ##### END ---- INFO-TAB ###### -->

<!--NEXT-->




</div>
    <!-- ##### START ---- CONTENT ###### -->

</div>
</body>
<script src="./dist/bootstrap.min.js"></script>
</html>