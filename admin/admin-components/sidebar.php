<!-- ##### START ---- NAVBAR ###### -->
<nav class="navbar ">
    <ul class="navbar-nav pl-2">
<!--        //Logo section ===START===-->
        <li class="nav-item ">
            <a href="" class="logo nav-link my-2  ">
                <div class="d-flex justify-content-between">
                    <img src="./icons/logo.png" alt="">
                    <img src="./icons/setting.png" alt="" class="" style="width: 16px; height: 16px;">
                </div>
                <span class="bar"></span>

            </a>
        </li>
<!--        // Logo section ===END===-->

        <li class="nav-item">
            <a class="nav-link disabled my-2 mx-1 no-accent" href="#"> список разделов</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#"><img class="nav-link-icon" src="./icons/1.png"  style="width:18px; height: 14px;" alt=""> Все разделы панели</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#"> <img class=" nav-link-icon" src="./icons/2.png"  style="width:18px; height: 14px;"  alt=""> Настройки сайта</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#"> <img class="nav-link-icon" src="./icons/3.png"  style="width:18px; height: 14px;"  alt="">Пользователи</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#"><img class="nav-link-icon" src="./icons/4.png"  style="width:18px; height: 14px;"  alt="">Портфолио</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="#"><img class="nav-link-icon" src="./icons/5.png" style="width:18px; height: 14px;" alt="">Блог</a>
        </li>

        <form action="engine/logout.php" method="POST">
            <li class="nav-item">
                <input type="submit" name="logout" value="LOGOUT">
            </li>
        </form>


    </ul>
</nav>
<!-- ##### END ---- NAVBAR ###### -->
