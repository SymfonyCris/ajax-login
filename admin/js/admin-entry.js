// инициализация переключения окон
var entry = document.querySelector('#js-admin-entry');
var forget = document.querySelector('#js-admin-forget');
var entryToggle = document.querySelector('#js-entry-toggle');
var forgetToggle = document.querySelector('#js-forget-toggle');

function changeWindow (evt) {
    evt.preventDefault();
    entry.classList.toggle('admin-entry__window--hide');
    forget.classList.toggle('admin-entry__window--hide');
}

entryToggle.addEventListener('click', changeWindow);
forgetToggle.addEventListener('click', changeWindow);

//инициализация инпутов

var inputs = document.querySelectorAll('.js-admin-input');

function onInputFocus (evt) {
    evt.target.nextElementSibling.classList.add('admin-input__placeholder--up');
    evt.target.addEventListener('blur', onInputBlur);
    evt.target.removeEventListener('focus', onInputFocus);
}

function onInputBlur (evt) {
    if (evt.target.value == '') {
        evt.target.nextElementSibling.classList.remove('admin-input__placeholder--up');
        evt.target.addEventListener('focus', onInputFocus);
    }
}

inputs.forEach((input) => {
    input.addEventListener('focus', onInputFocus);
});