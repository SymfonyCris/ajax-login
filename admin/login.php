<!DOCTYPE html>
<?php
session_start();
if(isset($_SESSION['authentificated']) &&  $_SESSION['authentificated'] ==  true ){
    header('Location: ./index.php');
}
?>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <title>Вход в панель управления</title>
    <link type="text/css" rel="stylesheet" href="css/style.min.css">
</head>
<svg style="display: none">
    <g id="check">
        <polygon points="16.12 2.12 14 0 5.56 8.44 2.12 5 0 7.12 5 12.12 5.56 11.56 6.12 12.12 16.12 2.12"></polygon>
    </g>
</svg>

<body class="admin-entry">
    <main class="admin-entry__wrapper">
        <section id="js-admin-entry" class="admin-entry__window">


            <form id="login-form" class="admin-entry__form" action="engine/authentification.php" method="POST">
                <div class="admin-entry__tab">
                    <h2 class="admin-entry__title">
                        Панель управления CreativeStudio Engine
                    </h2>

                </div>
                <div class="admin-entry__form-wrap">
                    <div class="admin-input__wrapper">
                        <input class="admin-input js-admin-input" type="login" name="email" >
                        <span class="admin-input__placeholder">E-mail</span>
                    </div>
                    <div class="admin-input__wrapper">
                        <input class="admin-input js-admin-input" type="password" name="password">
                        <span class="admin-input__placeholder">Пароль</span>
                    </div>
                    <label class="admin-checkbox__label">
                        <input class="visually-hidden admin-checkbox__native" type="checkbox" name="">
                        <span class="admin-checkbox">
                            <svg viewBox="0 0 18 12" class="admin-checkbox__check">
                                <use xlink:href="#check"></use>
                            </svg>
                        </span>
                        <span class="admin-checkbox__text">Чужой компьютер</span>
                    </label>
                    <input class="admin-button" type="submit" value="Войти">
                    <div class="admin-entry__bottom">
                        <a id="js-entry-toggle" href="#" class="admin-entry__link">Забыли пароль?</a>
                    </div>
                </div>
            </form>



        </section>
        <section id="js-admin-forget" class="admin-entry__window admin-entry__window--hide">
            <form id="forget-form" class="admin-entry__form" action="#" method="POST">
                <div class="admin-entry__tab">
                    <h2 class="admin-entry__title">Панель управления CreativeStudio Engine
                </h2></div>
                <div class="admin-entry__form-wrap">
                    <div class="admin-input__wrapper">
                        <input class="admin-input js-admin-input" type="email" name="email">
                        <span class="admin-input__placeholder">Ваш email</span>
                    </div>
                    <input type="submit" class="admin-button" value="Сбросить пароль">
                    <div class="admin-entry__bottom">
                        <a id="js-forget-toggle" href="#" class="admin-entry__link">Вход</a>
                    </div>
                </div>
            </form>
        </section>
    </main>
    <footer class="admin-footer">
        <div class="admin-footer__copyright">
            <p class="admin-footer__text">CreativeStudio Engine® Copyright 2018-2019</p>
            <p class="admin-footer__text">© <a href="#" class="admin-footer__link">Creative Studio</a> All rights
                reserved.</p>
        </div>
    </footer>

    <script type="text/javascript" src="js/admin-entry.js"></script>
    <script type="text/javascript" src="js/ajax.js"></script>
</body>
</html>